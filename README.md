# FY 2021-22


1. For **Outstanding Maintenance** see [Maintenance spreadsheet](https://docs.google.com/spreadsheets/d/11fsUnauqnH27pFXbjf6eiW0PCRqkh0__cD8hzKrK6Fs/edit?usp=sharing) first sheet has the details of current outstanding payments.

2. To record the payment transaction details use [Maintenance Form](https://forms.gle/tv2ha8ghDG39esLWA). This form updates same spreadsheet's sheet: (direct url) [payment information](https://docs.google.com/spreadsheets/d/11fsUnauqnH27pFXbjf6eiW0PCRqkh0__cD8hzKrK6Fs/edit#gid=586282933)

# Note:
Even thought the payments are recorded by residents, sheet is updated **ONLY** when the transaction is recorded in bank statement at the end of the month.

Spreadsheet has following pages:

- _Summary_ - Displays Yearly calculations in brief
- _Monthly Credit-Debit Statement_ and _Calculations_ - Day to day calculations
- _Configuration_ - contains configurables for the calculations like maintenance charges etc.
- _ResidentInformation_ information
- _Payments (cash + bank)_ bank records and assignments againt the flat
- _Opening balance as of 31st March 2021_ last year's opening balance
- _payments information_ recorded from [payment information google form](https://docs.google.com/spreadsheets/d/11fsUnauqnH27pFXbjf6eiW0PCRqkh0__cD8hzKrK6Fs/edit#gid=586282933)
- _Expences/Cash in hand_ current cash in hand status and expences done in cash by members%
